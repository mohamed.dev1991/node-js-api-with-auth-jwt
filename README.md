# Hello this is sample project by Nodejs and expressjs

<h3>Project modules</h3> 
<ul>
<li>Login</li>
<li>Registrations - JWT</li>
<li>List all users</li>
<li>List all tasks</li>
<li>Add new task</li>
<li>Edit task</li>
<li>List single task</li>
<li>Delete task</li>
</ul>

<h3>how it work</h3> 
<ul>
<li>clone repo</li>
<li>npm init --yes</li>
<li>update your .env file</li>
<li>npm run dev</li>
<li>all api under group route /api/v1</li>
</ul>
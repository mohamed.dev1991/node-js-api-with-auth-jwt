const users = require('./usersRoutes');
const tasks = require('./todoListRoutes'); 

module.exports = (router) => {
  users(router);
  tasks(router);
  return router;
};
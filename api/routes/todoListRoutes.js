'use strict';
module.exports = function(router) {
  const todoList = require('../controllers/todoListController');
  const validateToken = require('../../utils').validateToken;

  // todoList Routes
  router.route('/tasks')
    .get(validateToken,todoList.list_all_tasks)
    .post(validateToken,todoList.create_a_task);


    router.route('/tasks/:taskId')
    .get(validateToken,todoList.read_a_task)
    .put(validateToken,todoList.update_a_task)
    .delete(validateToken,todoList.delete_a_task);
};
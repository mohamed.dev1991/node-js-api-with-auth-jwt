'use strict';

module.exports = (router) => {
  const controller = require('../controllers/usersController');
  const validateToken = require('../../utils').validateToken;

    router.route('/users')
      .post(controller.add)
      .get(validateToken,controller.getAll);

      router.route('/login')
        .post(controller.login);
        
  };
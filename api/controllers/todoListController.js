'use strict';

const connUri = process.env.MONGO_LOCAL_CONN_URL;

var mongoose = require('mongoose');
const Task = require('../models/todoListModel');

exports.list_all_tasks = function(req, res) {
    mongoose.connect(connUri, { useNewUrlParser: true }, (err) => {
      let result = {};
      let status = 200;
      if (!err) {
          Task.find({}, (err, tasks) => {
            if (!err) {
              result.status = status;
              result.error = err;
              result.result = tasks;
            } else {
              status = 500;
              result.status = status;
              result.error = err;
            }
            res.status(status).send(result);
          });

      } else {
        status = 500;
        result.status = status;
        result.error = err;
        res.status(status).send(result);
      }
    });
};




exports.create_a_task = function(req, res) {
  mongoose.connect(connUri, { useNewUrlParser: true }, (err) => {
  var new_task = new Task(req.body);
  new_task.save(function(err, task) {
    let result = {};
    let status = 200;
    if (!err) {
      result.status = status;
      result.error = err;
      result.result = task;
    } else {
      status = 500;
      result.status = status;
      result.error = err;
    }
    res.status(status).send(result);
  });

});
};


exports.read_a_task = function(req, res) {
  mongoose.connect(connUri, { useNewUrlParser: true }, (err) => {
  Task.findById(req.params.taskId, function(err, task) {
    let result = {};
    let status = 200;
    if (!err) {
      result.status = status;
      result.error = err;
      result.result = task;
    } else {
      status = 500;
      result.status = status;
      result.error = err;
    }
    res.status(status).send(result);
  });
});
};


exports.update_a_task = function(req, res) {
  mongoose.connect(connUri, { useNewUrlParser: true }, (err) => {
  Task.findOneAndUpdate({_id: req.params.taskId}, req.body, {new: true}, function(err, task) {
    let result = {};
    let status = 200;
    if (!err) {
      result.status = status;
      result.error = err;
      result.result = task;
    } else {
      status = 500;
      result.status = status;
      result.error = err;
    }
    res.status(status).send(result);
  });
});
};


exports.delete_a_task = function(req, res) {
  mongoose.connect(connUri, { useNewUrlParser: true }, (err) => {

  Task.remove({
    _id: req.params.taskId
  }, function(err, task) {
    let result = {};
    let status = 200;
    if (!err) {
      result.status = status;
      result.error = err;
      result.result = task;
    } else {
      status = 500;
      result.status = status;
      result.error = err;
    }
    res.status(status).send(result);
  });
});
};

